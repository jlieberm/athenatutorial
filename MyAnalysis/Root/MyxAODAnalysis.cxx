#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name, ISvcLocator *pSvcLocator) 
	: EL::AnaAlgorithm(name, pSvcLocator) {
			declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0, "Minimum electron pT (in MeV)");
			declareProperty( "SampleName", m_sampleName = "Unknown", "Descriptive name for the processed sample" );
			
			// Here you put any code for the base initialization of variables,
			// e.g. initialize all pointers to 0.  This is also where you
			// declare all properties for your algorithm.  Note that things like
			// resetting statistics variables or booking histograms should
			// rather go into the initialize() function.
	}

StatusCode MyxAODAnalysis :: initialize () {

	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.
	ANA_MSG_INFO("in initialize");
	ANA_CHECK(book (TH1F("h_jetPt","h_jetPt",100,0,500)));
	ANA_MSG_INFO( "ElectronPtCut 	= " << m_electronPtCut );
	ANA_MSG_INFO( "SampleName 	= " << m_sampleName ); 
	return StatusCode::SUCCESS;
	}

StatusCode MyxAODAnalysis :: execute (){
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.
	ANA_MSG_INFO("in execute");
	const xAOD::EventInfo *eventInfo = nullptr;
	ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
	ANA_MSG_INFO("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
	const xAOD::JetContainer* jets = nullptr;
	ANA_CHECK(evtStore()->retrieve(jets, "AntiKt4EMTopoJets"));
	for (const xAOD::Jet *jet : *jets) {
		ANA_MSG_INFO ("execute(): jet pt = " << (jet->pt() * 0.001) << " GeV");
	}
	return StatusCode::SUCCESS;
	}


StatusCode MyxAODAnalysis :: finalize (){

	// This method is the mirror image of initialize(), meanining it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
       	// merged.
	return StatusCode::SUCCESS;
	}
