#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
class MyxAODAnalysis : public EL::AnaAlgorithm{

	public:
		MyxAODAnalysis(const std::string& name, ISvcLocator* pSvcLocator);

		virtual StatusCode initialize () override;
		virtual StatusCode execute () override;
		virtual StatusCode finalize () override;

	private:
		double m_electronPtCut;
		std::string m_sampleName;
		// float m_cutValue;
		// TTree *m_myTree;
		// TH1 *m_myHist;
	};

#endif
